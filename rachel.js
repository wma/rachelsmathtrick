$(function(){
    function getRandom(){
        return parseInt(Math.random() * 20 + 1);
    }
    function resetValues(){
        $('#two').text(getRandom());
        $('#three').text(getRandom());
        $('#right,#wrong').hide();
        $('#curious').show();

        $('#pic-question').hide();
        $('#text-question').show();
        
        $('#two-plus-three').val('').focus();
    }
    resetValues();


    $('#amIRight').click(function(){
        var rawValue = $('#two-plus-three').val();
        var value = parseInt(rawValue);
        var span2 = parseInt($('#two').text());
        var span3 = parseInt($('#three').text());
        console.log('span 2 + span 3: ' + (span2 + span3));
        if(value === (span2 + span3)){
            console.log("Right");
            $('#right').show();
            $('#wrong').hide();
        }else{
            console.log("Wrong");
            $('#right').hide();
            $('#wrong').show();
        }
        $('#curious').hide();
    });

    $('#nextTrick').click(resetValues);

    $('#many-cats').click(function(){
        function toggleShowOrHide(el){
            if(el.is(':visible')){
                el.hide();
            }else{
                el.show();
            }
        }
        toggleShowOrHide($('#pic-question'));
        toggleShowOrHide($('#text-question'));
    
    });
    //  alert("Hello there, this is Rachel's javascript");
});